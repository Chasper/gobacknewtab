chrome.commands.onCommand.addListener(function(command) {
  chrome.tabs.query({currentWindow: true, active: true}, function(tabs){
    chrome.tabs.duplicate(tabs[0].id, function(duplicate) {
      chrome.tabs.executeScript(duplicate.id, {code: 'history.back();'});
      chrome.tabs.query({index: duplicate.index - 1}, function(tabs){
        chrome.tabs.update(tabs[0].id, {active: true});
      });
    });
  });
});